#!/system/bin/sh
# Written by Draco (tytydraco @ GitHub)

# The name of the current branch for logging purposes
BRANCH="latency"

# Maximum unsigned integer size in C
UNIT_MAX="4294967295"

# Minimum frequencies
UNIT_MIN="51000"

# Duration in nanoseconds of one scheduling period
SCHED_PERIOD="10000000" # "$((10 * 1000 * 1000))"

# How many tasks should we have at a maximum in one scheduling period
SCHED_TASKS="10"

write() {
	# Bail out if file does not exist
	[[ ! -f "$1" ]] && return 1

	# Make file writable in case it is not already
	chmod +w "$1" 2> /dev/null

	# Write the new value and bail if there's an error
	if ! echo "$2" > "$1" 2> /dev/null
	then
		echo "Failed: $1 → $2"
		return 1
	fi

	# Log the success
	echo "$1 → $2"
}

# Check for root permissions and bail if not granted
if [[ "$(id -u)" -ne 0 ]]
then
	echo "No root permissions. Exiting."
	exit 1
fi

# Detect if we are running on Android
grep -q android /proc/cmdline && ANDROID=true

# Log the date and time for records sake
echo "Time of execution: $(date)"
echo "Branch: ${BRANCH}"

# Sync to data in the rare case a device crashes
sync

# Limit max perf event processing time to this much CPU usage
write /proc/sys/kernel/perf_cpu_time_max_percent 3

# Group tasks for less stutter but less throughput
write /proc/sys/kernel/sched_autogroup_enabled 1

# Execute child process before parent after fork
write /proc/sys/kernel/sched_child_runs_first 1 #0

# Preliminary requirement for the following values
write /proc/sys/kernel/sched_tunable_scaling 0

# Reduce the maximum scheduling period for lower latency
write /proc/sys/kernel/sched_latency_ns 1000000 # 10000000 "${SCHED_PERIOD}" 30000000

# Schedule this ratio of tasks in the guarenteed sched period
write /proc/sys/kernel/sched_min_granularity_ns 100000 # 2250000 "$((SCHED_PERIOD / SCHED_TASKS))" 3000000

# Require preeptive tasks to surpass half of a sched period in vmruntime
write /proc/sys/kernel/sched_wakeup_granularity_ns 500000 # 2000000 "$((SCHED_PERIOD / 2))" 6000000

# Reduce the frequency of task migrations
write /proc/sys/kernel/sched_migration_cost 5000000 # 500000 "$((SCHED_PERIOD / SCHED_TASKS / 6))" 500000

# Always allow sched boosting on top-app tasks
[[ "$ANDROID" == true ]] && write /proc/sys/kernel/sched_min_task_util_for_colocation 0

# Improve real time latencies by reducing the scheduler migration time
write /proc/sys/kernel/sched_nr_migrate 4

# Disable scheduler statistics to reduce overhead
write /proc/sys/kernel/sched_schedstats 0

# Disable unnecessary printk logging
write /proc/sys/kernel/printk_devkmsg off

#write /proc/sys/vm/dirty_background_bytes 20971520

#write /proc/sys/vm/dirty_bytes 41943040

# Start non-blocking writeback later
write /proc/sys/vm/dirty_background_ratio 3 # 5

# Start blocking writeback later
write /proc/sys/vm/dirty_ratio 30 # 15

# Require dirty memory to stay in memory for longer
write /proc/sys/vm/dirty_expire_centisecs 3000 # 200

# Run the dirty memory flusher threads less often
write /proc/sys/vm/dirty_writeback_centisecs 3000 # 400

# Disable read-ahead for swap devices
write /proc/sys/vm/page-cluster 0 # 1

# Update /proc/stat less often to reduce jitter
write /proc/sys/vm/stat_interval 10 #1

# Swap to the swap device at a fair rate
write /proc/sys/vm/swappiness 100

# Prioritize page cache over simple file structure nodes
write /proc/sys/vm/vfs_cache_pressure 200 # 100

# Allow LMK to free more RAM
write /proc/sys/vm/highmem_is_dirtyable 0

# Not usually clear cache
#write /proc/sys/vm/drop_caches 3

# Enable Explicit Congestion Control
write /proc/sys/net/ipv4/tcp_ecn 1

# Enable fast socket open for receiver and sender
write /proc/sys/net/ipv4/tcp_fastopen 3

# Disable SYN cookies
write /proc/sys/net/ipv4/tcp_syncookies 0

# Disable sysrq from keyboard, Marshmallow does this
write /proc/sys/kernel/sysrq 0

# GPU
# PRISM off
write /sys/devices/tegradc.0/smartdimmer/enable 0
# PRISM off (2)
setprop persist.tegra.didim.enable 0
# stop throttling gpu
write /sys/devices/host1x/gr3d/enable_3d_scaling 1

#enable miracast
setprop persist.debug.wfd.enable 1

#enable tethering even if carrier blocks it (tilapia)
setprop net.tethering.noprovisioning true

#MULTITASKING
# only for CM, can purge bitmaps
setprop persist.sys.purgeable_assets 1

# NO KSM, too cpu intensive, not much savings
write /sys/kernel/mm/ksm/run 0

# ram tuning
# https://01.org/android-ia/user-guides/android-memory-tuning-android-5.0-and-5.1
# these are from intel's recommendation for 1gb devices

setprop dalvik.vm.heapstartsize 8m
setprop dalvik.vm.heapgrowthlimit 64m
setprop dalvik.vm.heapsize 174m
setprop dalvik.vm.heaptargetutilization 0.75
setprop dalvik.vm.heapminfree 2m
setprop dalvik.vm.heapmaxfree 8m

# 1 day
settings put global fstrim_mandatory_interval 86400000
# effectively, never
settings put global storage_benchmark_interval 9223372036854775807

for f in /sys/fs/ext4/*; 
do 
  test "$f" = "/sys/fs/ext4/features" && continue 
  write "${f}/max_writeback_mb_bump" 128 # 8 don't spend too long writing ONE file if multiple need to write 
done

if test -e /sys/block/dm-0; then
  for f in /sys/block/dm-*; do # encrypted filesystems
    write "${f}/queue/rq_affinity" 2 # moving cpus is "expensive"
    write "${f}/queue/rotational" 0
  done
fi

if [[ -f "/sys/kernel/debug/sched_features" ]]
then
	# Consider scheduling tasks that are eager to run
	write /sys/kernel/debug/sched_features NEXT_BUDDY

	# Some sources report large latency spikes during large migrations
	write /sys/kernel/debug/sched_features NO_TTWU_QUEUE
fi

[[ "$ANDROID" == true ]] && if [[ -d "/dev/stune/" ]]
then
	# Prefer to schedule top-app tasks on idle CPUs
	write /dev/stune/top-app/schedtune.prefer_idle 1

	# Mark top-app as boosted, find high-performing CPUs
	write /dev/stune/top-app/schedtune.boost 1
fi

# Loop over each CPU in the system
for cpu in /sys/devices/system/cpu/cpu*/cpufreq
do
	# Fetch the available governors from the CPU
	avail_govs="$(cat "${cpu}/scaling_available_governors")"

	# Attempt to set the governor in this order
	for governor in intelliactive ondemand interactive conservative
	do
		# Once a matching governor is found, set it and break for this CPU
		if [[ "${avail_govs}" == *"${governor}"* ]]
		then
			write "${cpu}/scaling_governor" "${governor}"
			break
		fi
	done
done

# Apply governor specific tunables for intelliactive
find /sys/devices/system/cpu/ -name intelliactive -type d | while IFS= read -r governor
do
  	# CPU0:51000=0 102000=0 204000=0 370000=0 513000=0 620000=0 760000=0 860000=0 1000000=0 1100000=0 1200000=0 1300000=0
  	# Consider changing frequencies once per scheduling period
	write "${governor}/above_hispeed_delay" "20000 860000:40000 1100000:20000" # 20000
  	write "${governor}/boost" 0
  	write "${governor}/boostpulse_duration" 0
  	write "${governor}/io_is_busy" 1
  	write "${governor}/target_loads" "85 1000000:90 1200000:70" # 90
  	write "${governor}/timer_rate" 20000
  	write "${governor}/timer_slack" -1 # 80000
	write "${governor}/min_sample_time" 40000 # 80000
  	write "${governor}/sampling_down_factor" 100000
  	write "${governor}/sync_freq" 1100000

	# Jump to hispeed frequency at this load percentage
	write "${governor}/go_hispeed_load" 90
  	write "${governor}/hispeed_freq" 620000
  	write "${governor}/two_phase_freq" "620000,760000,1000000,1100000"
   	write "${governor}/up_threshold_any_cpu_freq" 1100000
  	write "${governor}/up_threshold_any_cpu_load" 85

  	#write /sys/module/cpuidle_t3/parameters/lp2_0_in_idle Y
  	write /sys/module/tegra3_clocks/parameters/detach_shared_bus Y
  	write /sys/module/tegra3_clocks/parameters/skipper_delay 0
  	write /sys/module/cpu_tegra/parameters/cpu_user_cap 4294967295
  	write /sys/module/cpu_tegra/parameters/force_policy_max Y

  	write /sys/devices/system/cpu/cpuquiet/balanced/balance_level 66
  	write /sys/devices/system/cpu/cpuquiet/balanced/down_delay 0
  	write /sys/devices/system/cpu/cpuquiet/balanced/idle_bottom_freq 620000
  	write /sys/devices/system/cpu/cpuquiet/balanced/idle_top_freq 1100000
  	write /sys/devices/system/cpu/cpuquiet/balanced/up_delay 0

  	write /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/down_delay 0
  	write /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/idle_bottom_freq 620000
  	write /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/idle_top_freq 1100000
  	write /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/up_delay 0

done

# Apply governor specific tunables for interactive
find /sys/devices/system/cpu/ -name interactive -type d | while IFS= read -r governor
do
	# Consider changing frequencies once per scheduling period
	write "${governor}/above_hispeed_delay" "30000 760000:20000 860000:10000" # 20000
  	write "${governor}/boost" 0
  	write "${governor}/boost_factor" 0
  	write "${governor}/core_lock_count" 2
  	write "${governor}/core_lock_period" 3000000
  	#write "${governor}/target_loads" "75 370000:69 513000:80 620000:76 760000:81 860000:69 1000000:78"
  	write "${governor}/timer_rate" 20000 # 50000
	write "${governor}/min_sample_time" 60000 # 500000
  	write "${governor}/input_boost" 1
  	write "${governor}/io_is_busy" 0
  	write "${governor}/max_boost" 0
  	write "${governor}/sustain_load" 80

	# Jump to hispeed frequency at this load percentage
	write "${governor}/go_hispeed_load" 75
  	write "${governor}/go_maxspeed_load" 85
	write "${governor}/hispeed_freq" 860000

done

# Apply governor specific tunables for conservative
find /sys/devices/system/cpu/ -name conservative -type d | while IFS= read -r governor
do
	# Consider changing frequencies once per scheduling period
	write "${governor}/down_threshold" 20
	write "${governor}/freq_step" 5
	write "${governor}/ignore_nice_load" 0
	write "${governor}/sampling_down_factor" 1
	write "${governor}/sampling_rate" 300000
	write "${governor}/sampling_rate_min" 200000

	# Jump to hispeed frequency at this load percentage
	write "${governor}/up_threshold" 80
done

# Apply governor specific tunables for ondemand
find /sys/devices/system/cpu/ -name ondemand -type d | while IFS= read -r governor
do
	# Consider changing frequencies once per scheduling period
  	write "${governor}/ignore_nice_load" 0
	write "${governor}/io_is_busy" 0
	write "${governor}/powersave_bias" 0
	write "${governor}/sampling_down_factor" 1
	write "${governor}/sampling_rate" 20000 #300000

	# Jump to hispeed frequency at this load percentage
	write "${governor}/up_threshold" 75

done

for queue in /sys/block/loop*/queue
do
	# Choose the first governor available
	avail_scheds="$(cat "${queue}/scheduler")"
	for sched in noop row [cfq] zen kyber bfq mq-deadline none
	do
		if [[ "${avail_scheds}" == *"${sched}"* ]]
		then
			write "${queue}/scheduler" "${sched}"
			break
		fi
	done

	# Do not use I/O as a source of randomness
  	chmod -R 0777 "${queue}"/add_random
	write "${queue}/add_random" 0

  	chmod -R 0777 "${queue}"/discard_granularity
	write "${queue}/discard_granularity" 0

  	chmod -R 0777 "${queue}"/discard_max_bytes
	write "${queue}/discard_max_bytes" 0

  	chmod -R 0777 "${queue}"/discard_zeroes_data
	write "${queue}/discard_zeroes_data" 0

  	chmod -R 0777 "${queue}"/hw_sector_size
	write "${queue}/hw_sector_size" 512

	# Disable I/O statistics accounting
  	chmod -R 0777 "${queue}"/iostats
	write "${queue}/iostats" 0

  	chmod -R 0777 "${queue}"/logical_block_size
	write "${queue}/logical_block_size" 512

  	chmod -R 0777 "${queue}"/max_hw_sectors_kb
	write "${queue}/max_hw_sectors_kb" 127 # 0

  	chmod -R 0777 "${queue}"/max_integrity_segments
	write "${queue}/max_integrity_segments" 0

  	chmod -R 0777 "${queue}"/max_sectors_kb
	write "${queue}/max_sectors_kb" 127 # 0

  	chmod -R 0777 "${queue}"/max_segment_size
	write "${queue}/max_segment_size" 65536 # 4096

  	chmod -R 0777 "${queue}"/max_segments
	write "${queue}/max_segments" 128 # 0

  	chmod -R 0777 "${queue}"/minimum_io_size
	write "${queue}/minimum_io_size" 512 # 0

  	chmod -R 0777 "${queue}"/nomerges
	write "${queue}/nomerges" 0

	# Reduce the maximum number of I/O requests in exchange for latency
  	chmod -R 0777 "${queue}"/nr_requests
	write "${queue}/nr_requests" 32 # 128

  	chmod -R 0777 "${queue}"/optimal_io_size
	write "${queue}/optimal_io_size" 0

  	chmod -R 0777 "${queue}"/physical_block_size
	write "${queue}/physical_block_size" 512 # 0

	# Reduce heuristic read-ahead in exchange for I/O latency
  	chmod -R 0777 "${queue}"/read_ahead_kb
	write "${queue}/read_ahead_kb" 32 # 128

  	chmod -R 0777 "${queue}"/rotational
	write "${queue}/rotational" 1

	# Move cpus is "expensive"
  	chmod -R 0777 "${queue}"/rq_affinity
	write "${queue}/rq_affinity" 0

	#write "${queue}/scheduler" none

done

for queue in /sys/block/mmcblk0/queue
do
	# Choose the first governor available
	avail_scheds="$(cat "${queue}/scheduler")"
	for sched in noop row [cfq] zen kyber bfq mq-deadline none
	do
		if [[ "${avail_scheds}" == *"${sched}"* ]]
		then
			write "${queue}/scheduler" "${sched}"
			break
		fi
	done

	# Do not use I/O as a source of randomness
  	chmod -R 0777 "${queue}"/add_random
	write "${queue}/add_random" 1

  	chmod -R 0777 "${queue}"/discard_granularity
	write "${queue}/discard_granularity" 4194304

  	chmod -R 0777 "${queue}"/discard_max_bytes
	write "${queue}/discard_max_bytes" 2199023255040

  	chmod -R 0777 "${queue}"/discard_zeroes_data
	write "${queue}/discard_zeroes_data" 0

  	chmod -R 0777 "${queue}"/hw_sector_size
	write "${queue}/hw_sector_size" 512

	# Disable I/O statistics accounting
  	chmod -R 0777 "${queue}"/iostats
	write "${queue}/iostats" 1

  	chmod -R 0777 "${queue}"/logical_block_size
	write "${queue}/logical_block_size" 512

  	chmod -R 0777 "${queue}"/max_hw_sectors_kb
	write "${queue}/max_hw_sectors_kb" 32767

  	chmod -R 0777 "${queue}"/max_integrity_segments
	write "${queue}/max_integrity_segments" 0

  	chmod -R 0777 "${queue}"/max_sectors_kb
	write "${queue}/max_sectors_kb" 512

  	chmod -R 0777 "${queue}"/max_segment_size
	write "${queue}/max_segment_size" 65535

  	chmod -R 0777 "${queue}"/max_segments
	write "${queue}/max_segments" 512

  	chmod -R 0777 "${queue}"/minimum_io_size
	write "${queue}/minimum_io_size" 512

  	chmod -R 0777 "${queue}"/nomerges
	write "${queue}/nomerges" 0

	# Reduce the maximum number of I/O requests in exchange for latency
  	chmod -R 0777 "${queue}"/nr_requests
	write "${queue}/nr_requests" 32 # 128

  	chmod -R 0777 "${queue}"/optimal_io_size
	write "${queue}/optimal_io_size" 0

  	chmod -R 0777 "${queue}"/physical_block_size
	write "${queue}/physical_block_size" 512

	# Reduce heuristic read-ahead in exchange for I/O latency
  	chmod -R 0777 "${queue}"/read_ahead_kb
	write "${queue}/read_ahead_kb" 32 # 128

  	chmod -R 0777 "${queue}"/rotational
	write "${queue}/rotational" 0

	# Move cpus is "expensive"
  	chmod -R 0777 "${queue}"/rq_affinity
	write "${queue}/rq_affinity" 1

	# Change scheduler to cfq
	write "${queue}/scheduler" cfq # "noop row [cfq] zen"

  write "${queue}"/iosched/back_seek_max 16384 # default 2147483647 i.e. the whole disk
 	write "${queue}"/iosched/back_seek_penalty 1 # no penalty
 	write "${queue}"/iosched/fifo_expire_async 250
 	write "${queue}"/iosched/fifo_expire_sync 120
  write "${queue}"/iosched/group_idle 0 # BUT make sure there is differentiation between cgroups
  write "${queue}"/iosched/low_latency 1
  write "${queue}"/iosched/quantum 8 # default 8. Removes bottleneck
  write "${queue}"/iosched/sync_async 40
  write "${queue}"/iosched/slice_async_rq 2 # default 2. See above
  write "${queue}"/iosched/slice_idle 0 # never idle WITHIN groups
  write "${queue}"/iosched/slice_sync 100
  write "${queue}"/iosched/target_latency 300

done

for queue in /sys/block/mmcblk0/mmcblk0boot*/queue
do
	# Choose the first governor available
	avail_scheds="$(cat "${queue}/scheduler")"
	for sched in noop row [cfq] zen kyber bfq mq-deadline none
	do
		if [[ "${avail_scheds}" == *"${sched}"* ]]
		then
			write "${queue}/scheduler" "${sched}"
			break
		fi
	done

	# Do not use I/O as a source of randomness
  	chmod -R 0777 "${queue}"/add_random
	write "${queue}/add_random" 1

  	chmod -R 0777 "${queue}"/discard_granularity
	write "${queue}/discard_granularity" 4194304

  	chmod -R 0777 "${queue}"/discard_max_bytes
	write "${queue}/discard_max_bytes" 2199023255040

  	chmod -R 0777 "${queue}"/discard_zeroes_data
	write "${queue}/discard_zeroes_data" 0

  	chmod -R 0777 "${queue}"/hw_sector_size
	write "${queue}/hw_sector_size" 512

	# Disable I/O statistics accounting
  	chmod -R 0777 "${queue}"/iostats
	write "${queue}/iostats" 1

  	chmod -R 0777 "${queue}"/logical_block_size
	write "${queue}/logical_block_size" 512

  	chmod -R 0777 "${queue}"/max_hw_sectors_kb
	write "${queue}/max_hw_sectors_kb" 32767

  	chmod -R 0777 "${queue}"/max_integrity_segments
	write "${queue}/max_integrity_segments" 0

  	chmod -R 0777 "${queue}"/max_sectors_kb
	write "${queue}/max_sectors_kb" 512

  	chmod -R 0777 "${queue}"/max_segment_size
	write "${queue}/max_segment_size" 65535

  	chmod -R 0777 "${queue}"/max_segments
	write "${queue}/max_segments" 512

  	chmod -R 0777 "${queue}"/minimum_io_size
	write "${queue}/minimum_io_size" 512

  	chmod -R 0777 "${queue}"/nomerges
	write "${queue}/nomerges" 0

	# Reduce the maximum number of I/O requests in exchange for latency
  	chmod -R 0777 "${queue}"/nr_requests
	write "${queue}/nr_requests" 32 # 128

  	chmod -R 0777 "${queue}"/optimal_io_size
	write "${queue}/optimal_io_size" 0

  	chmod -R 0777 "${queue}"/physical_block_size
	write "${queue}/physical_block_size" 512

	# Reduce heuristic read-ahead in exchange for I/O latency
  	chmod -R 0777 "${queue}"/read_ahead_kb
	write "${queue}/read_ahead_kb" 32 # 128

  	chmod -R 0777 "${queue}"/rotational
	write "${queue}/rotational" 0

	# Move cpus is "expensive"
  	chmod -R 0777 "${queue}"/rq_affinity
	write "${queue}/rq_affinity" 1

	# Change scheduler to cfq
	write "${queue}/scheduler" cfq # "noop row [cfq] zen"

  write "${queue}"/iosched/back_seek_max 16384 # default 2147483647 i.e. the whole disk
 	write "${queue}"/iosched/back_seek_penalty 1 # no penalty
 	write "${queue}"/iosched/fifo_expire_async 250
 	write "${queue}"/iosched/fifo_expire_sync 120
  write "${queue}"/iosched/group_idle 0 # BUT make sure there is differentiation between cgroups
  write "${queue}"/iosched/low_latency 1
  write "${queue}"/iosched/quantum 8 # default 8. Removes bottleneck
  write "${queue}"/iosched/sync_async 40
  write "${queue}"/iosched/slice_async_rq 2 # default 2. See above
  write "${queue}"/iosched/slice_idle 0 # never idle WITHIN groups
  write "${queue}"/iosched/slice_sync 100
  write "${queue}"/iosched/target_latency 300

done

for queue in /sys/block/zram*/queue
do
	# Choose the first governor available
	avail_scheds="$(cat "${queue}/scheduler")"
	for sched in noop row [cfq] zen kyber bfq mq-deadline none
	do
		if [[ "${avail_scheds}" == *"${sched}"* ]]
		then
			write "${queue}/scheduler" "${sched}"
			break
		fi
	done

	# Do not use I/O as a source of randomness
  	chmod -R 0777 "${queue}"/add_random
	write "${queue}/add_random" 0

  	chmod -R 0777 "${queue}"/discard_granularity
	write "${queue}/discard_granularity" 0 # 4096

  	chmod -R 0777 "${queue}"/discard_max_bytes
	write "${queue}/discard_max_bytes" 0 # 2199023255040

  	chmod -R 0777 "${queue}"/discard_zeroes_data
	write "${queue}/discard_zeroes_data" 0 # 1

  	chmod -R 0777 "${queue}"/hw_sector_size
	write "${queue}/hw_sector_size" 4096

	# Disable I/O statistics accounting
  	chmod -R 0777 "${queue}"/iostats
	write "${queue}/iostats" 0

  	chmod -R 0777 "${queue}"/logical_block_size
	write "${queue}/logical_block_size" 4096

  	chmod -R 0777 "${queue}"/max_hw_sectors_kb
	write "${queue}/max_hw_sectors_kb" 127

  	chmod -R 0777 "${queue}"/max_integrity_segments
	write "${queue}/max_integrity_segments" 0

  	chmod -R 0777 "${queue}"/max_sectors_kb
	write "${queue}/max_sectors_kb" 127

  	chmod -R 0777 "${queue}"/max_segment_size
	write "${queue}/max_segment_size" 65536

  	chmod -R 0777 "${queue}"/max_segments
	write "${queue}/max_segments" 128

  	chmod -R 0777 "${queue}"/minimum_io_size
	write "${queue}/minimum_io_size" 4096

  	chmod -R 0777 "${queue}"/nomerges
	write "${queue}/nomerges" 0

	# Reduce the maximum number of I/O requests in exchange for latency
  	chmod -R 0777 "${queue}"/nr_requests
	write "${queue}/nr_requests" 32 # 128

  	chmod -R 0777 "${queue}"/optimal_io_size
	write "${queue}/optimal_io_size" 4096

  	chmod -R 0777 "${queue}"/physical_block_size
	write "${queue}/physical_block_size" 4096

	# Reduce heuristic read-ahead in exchange for I/O latency
  	chmod -R 0777 "${queue}"/read_ahead_kb
	write "${queue}/read_ahead_kb" 32 # 128

  	chmod -R 0777 "${queue}"/rotational
	write "${queue}/rotational" 1

	# Move cpus is "expensive"
  	chmod 0777 "${queue}"/rq_affinity
	write "${queue}/rq_affinity" 0

	#write "${queue}/scheduler" none

done

#bb=/sbin/busybox

# for fixing audio stutter when multitasking
# increase priority of audio, decrease priority of eMMC *when something else is using the CPU ONLY*

#$bb renice -10 $($bb pidof hd-audio0)
#$bb ionice -c 1 -n 2 -p $($bb pidof hd-audio0)

#$bb renice 5 $($bb pidof mmcqd/0)
#$bb ionice -c 2 -n 4 -p $($bb pidof mmcqd/0) # to stop auto ionice from renice

chmod -R 0777 /sys/module/lowmemorykiller/parameters
write /sys/module/lowmemorykiller/parameters/adj "0,100,200,300,900,906" # default "0,58,117,235,411,1000" https://android.googlesource.com/platform/frameworks/base/+/master/services/core/java/com/android/server/am/ProcessList.java#50
write /sys/module/lowmemorykiller/parameters/adj_max_shift 606
write /sys/module/lowmemorykiller/parameters/minfree 8192,10240,12288,14336,16384,20480 # sm-t355y "12288,15360,18432,21504,24576,30720" - "8192,10240,12288,14336,16384,20480" # the same as Moto G 5.1, and AOSP 4.x - "18432,23040,27648,32256,36864,46080" LineageOS 7.1.2
write /sys/module/lowmemorykiller/parameters/cost 32 # default 48
write /sys/module/lowmemorykiller/parameters/debug_level 1
write /sys/module/lowmemorykiller/parameters/enable_adaptive_lmk 0
write /sys/module/lowmemorykiller/parameters/lmkcount 0
write /sys/module/lowmemorykiller/parameters/vmpressure_file_min 53059
chmod -R 0444 /sys/module/lowmemorykiller/parameters # so android can't edit it

# default from init.grouper.rc some additional performance tweaks (to be tested)
#write /sys/module/lowmemorykiller/parameters/adj "0,300,600,800,900,906"
#write /sys/module/lowmemorykiller/parameters/minfree "15360,19200,23040,26880,34415,43737"
#write /sys/module/lowmemorykiller/parameters/cost 48

for queue in /sys/block/loop*/bdi
do

	write "${queue}/read_ahead_kb" 64 # 128

done

for queue in /sys/block/mmcblk0/bdi
do

	write "${queue}/read_ahead_kb" 128 # 256

done

for queue in /sys/block/mmcblk0boot*/bdi
do

	write "${queue}/read_ahead_kb" 64 # 128

done

for queue in /sys/block/zram0/bdi
do

	write "${queue}/read_ahead_kb" 64 # 128

done

sysctl -p

# Always return success, even if the last write fails
exit 0
